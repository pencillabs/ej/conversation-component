import {
  Component,
  Event,
  EventEmitter,
  Prop,
  h,
  Element,
} from "@stencil/core";
import { User } from "../conversation/api/user";
import { HTMLStencilElement } from "@stencil/core/internal";

@Component({
  tag: "ej-conversation-lgpd",
  styleUrls: ["./main.css"],
  shadow: true,
})
export class EjConversationLGPD {
  // Indicate that name should be a public property on the component
  @Element() el!: HTMLStencilElement;
  @Prop() currentContainer: number = 0;
  @Prop() currentStep: number = 0;
  @Prop() theme: string = "icd";
  @Prop({ mutable: true }) componentIsVisible: boolean = false;
  @Event({
    bubbles: true,
    composed: true,
  })
  closeBoard: EventEmitter;
  lgpdAdviseText: string = "";
  termsOfUseURL: string = "https://www.ejparticipe.org/usage/";

  /* *
   * componentDidRender runs every time a @Prop is updated.
   * useful to manage component state.
   */
  async componentWillLoad() {
    if (!User.isAuthenticated()) {
      this.componentIsVisible = true;
    }
  }

  skip() {
    let board: HTMLElement = this.el.querySelector(".board");
    board.style.display = "none";
    this.userCheckedBoard();
    this.closeBoard.emit();
  }

  userCheckedBoard() {
    localStorage.setItem("userSawBoard", "yes");
  }

  showBoard() {
    if (localStorage.getItem("userSawBoard")) {
      return false;
    }
    return true;
  }

  lgpdAgree() {
    localStorage.setItem("lgpd", "agree");
    this.componentIsVisible = false;
  }

  lgpdDisagree() {
    localStorage.setItem("lgpd", "disagree");
    this.componentIsVisible = false;
  }

  getComponent() {
    return (
      <div class="board">
        <div class="background"></div>
        <div class="modal modal-lgpd">
          <div id="container0">
            <div class={"board-header " + `board-header-${this.theme}`}>
              <div class="lgpd">
                <p>
                  Nós utilizamos alguns cookies de terceiros com o fim de
                  melhorar a eficiência da ferramenta. Os dados coletados não
                  serão compartilhados com terceiros e serão usados apenas para
                  a finalidade de gerar visualizações agregadas e melhorar a
                  comunicação com os participantes dentro da finalidade da
                  ferramenta.
                </p>
                <span>
                  Para obter mais detalhes, acesse os&nbsp;
                  <a class="link" target="_blank" href={this.termsOfUseURL}>
                    termos de uso da plataforma.
                  </a>
                </span>
              </div>
              <div class="lgpd-card buttons">
                <div class="action-button">
                  <button onClick={() => this.lgpdAgree()}>
                    aceito e vou responder
                  </button>
                </div>
                <div class="close-card form__button action-button">
                  <button onClick={() => this.lgpdDisagree()}>
                    não quero participar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        {!User.isAuthenticated() &&
          this.componentIsVisible &&
          this.getComponent()}
      </div>
    );
  }
}
