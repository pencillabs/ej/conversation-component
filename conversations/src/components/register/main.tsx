import {
  Component,
  Prop,
  h,
  Element,
  getAssetPath,
  State,
  Event,
  EventEmitter,
  Listen,
} from "@stencil/core";
import { API } from "../conversation/api/api";
import { User } from "../conversation/api/user";
import { HTMLStencilElement } from "@stencil/core/internal";
import { CustomFields } from "../conversation/main";

@Component({
  tag: "ej-conversation-register",
  styleUrls: ["main.css"],
  assetsDirs: ["assets"],
  shadow: true,
})
export class EjConversationRegister {
  @Prop({ mutable: true }) componentIsVisible: boolean = true;
  @Element() el!: HTMLStencilElement;
  @Prop() conversation: any = {};
  @Prop() host: string;
  //conversation_id
  @Prop() cid: string;
  @Prop() comment: any = {};
  @Prop() newCommentContent: string = "";
  @Prop() api: API;
  @Prop() user: User = new User();
  @Event({
    bubbles: true,
    composed: true,
  })
  @Prop()
  customFields: CustomFields;
  @Prop() userNameErrors: string = "";
  emailInput: HTMLInputElement = null;
  otherEmailButton: HTMLAnchorElement = null;
  errorsTranslation: any = {
    "Error: Ensure this field has at least 5 characters.":
      "Seu nome deve ter no mínimo 5 caracteres.",
  };

  @Event({
    eventName: "userIsRegistered",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  registered: EventEmitter;

  @Event({
    eventName: "renderLogin",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  renderLoginComponentEmitter: EventEmitter;

  private renderLoginComponent() {
    this.componentIsVisible = false;
    this.renderLoginComponentEmitter.emit({
      api: this.api,
      user: this.user,
    });
  }

  @State() password: string = "";
  @State() passwordConfirm: string = "";
  PASSWORD_MAX_LENGTH = 128;

  @State() apiResponseError: string = "";
  @State() emailErrorMessage: string = "";

  componentDidRender() {
    this.getEmailFromURL();
  }

  @Listen("renderRegister", { target: "window" })
  async openComponentHandler(_: CustomEvent<any>) {
    this.componentIsVisible = true;
  }

  private async setUserEmail(event: any, email: string = "") {
    if (email != "") {
      this.user.email = email;
    } else {
      this.user.email = event.target.value;
    }
    this.user.name = this.user.email;

    if (event.target.validity.typeMismatch) {
      this.emailErrorMessage = "Insira um e-mail válido.";
    } else {
      this.emailErrorMessage = "";
    }
  }

  private async setUserPassword(event: any) {
    this.user.password = event.target.value;
    this.password = event.target.value;
  }

  private async setUserPasswordConfirm(event: any) {
    this.user.passwordConfirm = event.target.value;
    this.passwordConfirm = event.target.value;
  }

  private passwordsAreEqual(password: string, passwordConfirm: string) {
    return password == passwordConfirm;
  }

  private passwordLengthIsCorrect(password: string, passwordConfirm: string) {
    return (
      password.length <= this.PASSWORD_MAX_LENGTH &&
      passwordConfirm.length <= this.PASSWORD_MAX_LENGTH
    );
  }

  private validatePassword() {
    let password = this.password;
    let passwordConfirm = this.passwordConfirm;
    return (
      this.passwordsAreEqual(password, passwordConfirm) &&
      this.passwordLengthIsCorrect(password, passwordConfirm)
    );
  }

  private validateEmail() {
    const mailFormat =
      /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return this.user.email.match(mailFormat);
  }

  private async registerUser() {
    if (!this.user.email || !this.user.password || !this.user.passwordConfirm) {
      return;
    }
    if (!this.validatePassword() || !this.validateEmail()) {
      return;
    }
    try {
      if (this.user.email && this.user.password) {
        let response = await this.api.createUser(this.user);
        this.user.token = response.token;
        this.user.save();
      }
      this.componentIsVisible = false;
      this.registered.emit();
    } catch (error) {
      if ("apiError" in error) {
        this.apiResponseError = error.apiError;
      } else {
        this.apiResponseError = "não foi possível registrar o usuário.";
      }
    }
  }

  private getEmailFromURL(): string {
    const urlParams = new URLSearchParams(document.location.search);
    return urlParams.get("email");
  }

  private getEmailInput() {
    let emailFromURL: string = this.getEmailFromURL();
    const mailFormat =
      /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (emailFromURL && emailFromURL.match(mailFormat)) {
      this.setUserEmail(null, emailFromURL);
      return (
        <div class="form__field">
          <div>
            <label htmlFor="email">e-mail</label>
          </div>
          <div class="flex">
            <span>
              <i>{emailFromURL}</i>
            </span>
          </div>
        </div>
      );
    }
    return (
      <div class="form__field">
        <div>
          <label htmlFor="email">e-mail</label>
        </div>
        <div class="flex">
          <input
            onKeyUp={(event: UIEvent) => {
              this.setUserEmail(event);
              this.enableSubmit();
            }}
            type="email"
            id="email"
            placeholder="ex: nome@mail.com"
            ref={(el) => (this.emailInput = el as HTMLInputElement)}
            class="required"
          />
        </div>
        <div>
          {this.emailErrorMessage && (
            <span class="error-message">{this.emailErrorMessage}</span>
          )}
        </div>
      </div>
    );
  }

  private isButtonDisabled(input: HTMLInputElement) {
    return (
      input.value == "" ||
      input.value == null ||
      input.value == undefined ||
      !this.validatePassword() ||
      !this.validateEmail()
    );
  }

  private enableSubmit() {
    let inputs: NodeListOf<HTMLInputElement> =
      this.el.shadowRoot.querySelectorAll(".required");
    let submitButton: HTMLButtonElement = this.el.shadowRoot.getElementById(
      "submit_button"
    ) as HTMLButtonElement;
    let disabled = false;

    submitButton.classList.remove(
      "button--disabled",
      "action-button--disabled"
    );

    for (var i = 0; i < inputs.length; i++) {
      let input = inputs[i];
      if (this.isButtonDisabled(input)) {
        disabled = true;
        submitButton.classList.add(
          "button--disabled",
          "action-button--disabled"
        );
        break;
      }
    }

    submitButton.disabled = disabled;
  }

  private getComponent() {
    return (
      <div>
        <ej-card size="small">
          <div class="register-form">
            <div class="register-form__title">
              <h2> Registre-se para participar.</h2>
              <div class="register-form__subtitle">
                <h3>
                  A Empurrando Juntas (EJ) é uma plataforma de coleta de opinião
                  coletiva.
                </h3>
              </div>
            </div>
            <div class="register-card card">
              <form>
                <div class="form__field">{this.getEmailInput()}</div>
                <div class="form__field">
                  <div>
                    <label htmlFor="password">cadastre uma senha</label>
                  </div>
                  <div class="flex flex-justify-content-between">
                    <input
                      onKeyUp={(event: UIEvent) => {
                        this.setUserPassword(event);
                        this.enableSubmit();
                      }}
                      type="password"
                      id="password"
                      placeholder="insira uma senha"
                      class="required"
                    />
                    <input
                      onKeyUp={(event: UIEvent) => {
                        this.setUserPasswordConfirm(event);
                        this.enableSubmit();
                      }}
                      type="password"
                      id="password-confirmation"
                      placeholder="insira a senha novamente"
                      class="required"
                    />
                  </div>
                  {this.passwordConfirm &&
                    !this.passwordsAreEqual(
                      this.password,
                      this.passwordConfirm
                    ) && (
                      <span class="error-message">
                        As senhas precisam ser iguais.
                      </span>
                    )}
                  <br />
                  {this.passwordConfirm &&
                    !this.passwordLengthIsCorrect(
                      this.password,
                      this.passwordConfirm
                    ) && (
                      <span class="error-message">
                        O tamanho da senha não pode ultrapassar 128 caracteres.
                      </span>
                    )}
                  {this.apiResponseError && (
                    <span class="error-message">{this.apiResponseError}</span>
                  )}
                </div>
                <div class="flex flex-justify-content-between action-button">
                  <button
                    class="button--white"
                    onClick={() => this.renderLoginComponent()}
                    type="button"
                  >
                    Já sou usuário
                  </button>
                  <button
                    class="button--disabled action-button--disabled"
                    onClick={() => this.registerUser()}
                    id="submit_button"
                    type="button"
                    disabled
                  >
                    Participar
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div class="logo">
            <a href="https://sobre.ejparticipe.org" target="_blank">
              <img src={getAssetPath("assets/logo.png")} alt="" />
            </a>
            <a target="_blank">
              <img src={this.customFields["logo_image_url"]} alt="" />
            </a>
          </div>
        </ej-card>
      </div>
    );
  }

  render() {
    return (
      <div>
        {!User.isAuthenticated() &&
          this.componentIsVisible &&
          this.getComponent()}
      </div>
    );
  }
}
