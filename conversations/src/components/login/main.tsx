import { Component, Prop, Event, h, Element, Listen, EventEmitter, getAssetPath, State } from "@stencil/core";
import { API } from "../conversation/api/api";
import { User } from "../conversation/api/user";
import { HTMLStencilElement } from "@stencil/core/internal";

@Component({
  tag: "ej-conversation-login",
  styleUrls: ["main.css"],
})
export class EjConversationLogin {
  @Element() el!: HTMLStencilElement;
  @Prop({ mutable: true }) componentIsVisible: boolean = false;
  @Prop() host: string;
  @Prop() api: API;
  @Prop() user: User = new User();
  @Event({
    bubbles: true,
    composed: true,
  })
  @Prop() userNameErrors: string = "";

  @State() errorMessage: string = "";
  
  @Event({
    eventName: "userIsAuthenticated",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  authenticated: EventEmitter;

  @Event({
    eventName: "renderRegister",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  renderRegisterComponentEmitter: EventEmitter;

  email: string = "";
  password: string = "";

  private renderRegisterComponent() {
    this.componentIsVisible = false;
    this.renderRegisterComponentEmitter.emit({
      api: this.api,
      user: this.user,
    });
  }

  @Listen("renderLogin", { target: "window" })
  async openComponentHandler(_: CustomEvent<any>) {
    this.componentIsVisible = true;
  }

  private async setUserEmail(event: any) {
    this.email = event.target.value;
  }

  private async setUserPassword(event: any) {
    this.password = event.target.value;
  }

  private async authenticateUser() {
    try {
      if (!this.email||!this.password) {
        this.errorMessage = "As credenciais estão faltando.";
        return;
      }

      let response = await this.api.authenticateUser(this.email, this.password);
      
      if ("token" in response) {
        this.user = new User(this.email, this.password, response.token);
        this.user.save();
        this.componentIsVisible = false;
        this.authenticated.emit();
      }
  
    }
    catch (error) {
      if ("apiError" in error) {
        this.errorMessage = error.apiError;
      } else {
        this.errorMessage = "não foi possível autenticar o usuário.";
      }

    }
  }

  private getEmailInput() {
    return (
      <div class="form__field">
        <div>
          <label htmlFor="email">e-mail</label>
        </div>
        <div class="flex">
          <input
            onChange={(event: UIEvent) => this.setUserEmail(event)}
            type="email"
            id="email"
            placeholder="ex.: nome@email.com"
            value={this.email}
            required
          />
        </div>
      </div>
    );
  }

  private getComponent() {
    return (
      <div>
        <ej-card size="small">
          <div class="register-form">
            <div class="register-form__return-icon">
              <a onClick={() => this.renderRegisterComponent()}><i class="fa-solid fa-arrow-left"></i></a>
            </div>
            <div class="register-form__title">
              <h2>Login</h2>
            </div>
            <div class="register-card card">
              <form>
                <div class="form__field">{this.getEmailInput()}</div>
                <div class="form__field">
                  <div>
                    <label htmlFor="password">senha</label>
                  </div>
                  <div>
                    <input
                      class="login-password"
                      onChange={(event: UIEvent) => this.setUserPassword(event)}
                      type="password"
                      id="password"
                      placeholder="insira sua senha"
                      value={this.password}
                      required
                    />
                  </div>
                  <div>{this.errorMessage && (<span class="error-message">{this.errorMessage}</span>)}</div>
                </div>
                <div class="form__button action-button">
                  <a href={this.api.config.RECOVER_PASSWORD_ROUTE} class="form__href">Esqueci minha senha</a>
                  <button onClick={() => this.authenticateUser()} type="button">
                    Entrar
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div class="logo">
            <a href="https://sobre.ejparticipe.org" target="_blank">
              <img src={getAssetPath("assets/logo.png")} alt="Ej letters logo" />
            </a>
          </div>
        </ej-card>
      </div>
    );
  }

  render() {
    return <div>{!User.isAuthenticated() && this.componentIsVisible && this.getComponent()}</div>;
  }
}
