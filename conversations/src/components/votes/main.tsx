import { Component, Prop, h, Element } from "@stencil/core";
import { Listen } from "@stencil/core";
import { API } from "../conversation/api/api";
import { User } from "../conversation/api/user";
import { HTMLStencilElement } from "@stencil/core/internal";
import { Conversation } from "../conversation/api/conversation";
import { Event, EventEmitter } from "@stencil/core";
import { Comment } from "../conversation/api/comment";
import { CustomFields, QueryParams } from "../conversation/main";

@Component({
  tag: "ej-conversation-votes",
  styleUrls: ["./main.css"],
  shadow: true,
  assetsDirs: ["assets"],
})
export class EjConversationVotes {
  @Prop() api: API;
  @Element() el!: HTMLStencilElement;
  @Prop({ mutable: true }) conversation: Conversation;
  @Prop({ mutable: true }) componentIsVisible: boolean = false;
  @Prop() host: string;
  @Prop({ mutable: true }) comment: Comment = null;
  @Prop() user: User;
  @Prop() ejQueryParams: any;
  @Prop() LGPDDenied: boolean = false;
  @Prop() userStatistics: any;
  @Prop() childIsVisible: boolean = false;
  @Prop() readyToRender: boolean = false;
  @Prop() queryParams: QueryParams;
  @Prop({ mutable: true }) participationPercentage: string;
  @Prop({ mutable: true }) noCommentsToVote: boolean = false;
  @Prop({ mutable: true }) commentHeight: number;
  @Prop() customFields: CustomFields;
  @Prop() DEFAULT_GOODBY_MSG: string =
    "Você votou em todos os comentários. Obrigado pela sua participação.";

  @Event({
    eventName: "renderCommentComponent",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  renderComponent: EventEmitter;

  @Event({
    eventName: "voted",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  voted: EventEmitter;

  votedHandler(choice: string) {
    this.voted.emit(choice);
  }

  private renderCommentsComponent() {
    this.componentIsVisible = false;
    this.renderComponent.emit({
      conversation: this.conversation,
      user: this.user,
    });
  }

  @Listen("renderVotesComponent", { target: "window" })
  async openComponentHandler() {
    await this.setComponentState();
    this.commentHeight = 0;
    this.componentIsVisible = true;
    this.childIsVisible = true;
  }

  @Listen("userIsRegistered", { target: "window" })
  async userIsRegisteredHandler() {
    await this.setComponentVisible();
  }

  @Listen("userIsAuthenticated", { target: "window" })
  async userIsAuthenticatedHandler() {
    await this.setComponentVisible();
  }

  @Listen("voteButtonPressed", { target: "window" })
  async voteButtonPressedHandler(event: CustomEvent<string>) {
    const voteChoice = event.detail;
    await this.vote(voteChoice);
  }

  private async setComponentVisible() {
    await this.setComponentState();
    this.readyToRender = true;
    this.componentIsVisible = true;
    this.childIsVisible = true;
  }

  private async setComponentState() {
    let { response } = await this.api.getConversation();
    let { text, statistics } = response;
    this.conversation = new Conversation(text, statistics);
    this.comment = {
      ...(await this.api.getConversationNextComment()),
    };
    await this.setUserParticipation();
    this.voteUsingQueryParams();
  }

  private voteUsingQueryParams() {
    if (
      this.queryParams.votingChoice &&
      this.queryParams.commentId == this.comment.id
    ) {
      setTimeout(() => {
        this.vote(this.queryParams.votingChoice);
      }, 4000);
    }
  }

  /* *
   * componentWillLoad runs before render()
   * useful to load initial data from EJ API.
   */
  async componentWillLoad() {
    if (User.isAuthenticated() && this.api) {
      await this.setComponentState();
      this.readyToRender = true;
      this.childIsVisible = true;
      this.componentIsVisible = true;
    }
  }

  /* *
   * componentDidRender runs every time a @Prop is updated.
   * useful to manage component state.
   */
  async componentDidRender() {
    if (User.isAuthenticated() && this.readyToRender) {
      this.updatesProgressBar();
      this.resetNextCommentAnimation();
      if (!this.comment.content) {
        this.noCommentsToVote = true;
      }
      setTimeout(() => {
        if (this.noCommentsToVote) {
          this.hideParticipationButtons();
        }
        this.setNextCommentAnimation();
      }, 500);
    }
  }

  private hideParticipationButtons() {
    let buttons: HTMLElement = this.el.shadowRoot.querySelector(
      ".conversation__buttons"
    );
    buttons.style.display = "none";
  }

  private async setUserParticipation() {
    this.user.participation = {
      ...(await this.api.getUserConversationStatistics()),
    };
    this.setParticipationPercentage();
  }

  private async vote(choice: string) {
    this.childIsVisible = false;
    try {
      await this.api.vote(this.comment, choice);
      setTimeout(() => {
        this.votedHandler(choice);
      }, 1000);
    } catch (e) {
      console.log("could not compute vote");
    }
    await this.setUserParticipation();
    this.comment = { ...(await this.api.getConversationNextComment()) };
  }

  private updatesProgressBar() {
    let participationRatio: number =
      this.user.participation.participation_ratio;
    if (participationRatio == 0) {
      return;
    }
    let stepWidth: number = Math.round(99 * participationRatio);
    let progressBar: HTMLElement = this.el.shadowRoot.querySelector(
      ".progress-bar__progress"
    );
    if (progressBar) {
      progressBar.style.width = `${stepWidth}px`;
    }
  }

  private setParticipationPercentage() {
    if (
      this.user.participation.votes == 0 ||
      this.conversation.statistics.comments.total == 0
    ) {
      this.participationPercentage = "0";
    }
    this.participationPercentage = Math.round(
      (this.user.participation.comments /
        this.conversation.statistics.comments.approved) *
        100
    ).toFixed(0);
  }

  private getVotedCommentsCount() {
    if (
      this.user.participation.votes + 1 >
      this.user.participation.total_comments
    ) {
      return this.user.participation.total_comments;
    }
    return this.user.participation.votes + 1;
  }

  private getParticipationComments() {
    return `${this.getVotedCommentsCount()} de ${
      this.user.participation.total_comments
    } comentários`;
  }

  private setNextCommentAnimation() {
    let comment: HTMLElement = this.el.shadowRoot.querySelector(
      ".conversation__comment"
    );
    let commentContent: HTMLElement = this.el.shadowRoot.querySelector(
      ".conversation__comment span"
    );
    if (commentContent) {
      comment.style.height = `${commentContent.offsetHeight}px`;
      this.commentHeight = commentContent.offsetHeight;
      commentContent.classList.add("conversation__comment--translate");
    }
  }

  private resetNextCommentAnimation() {
    let commentContent: HTMLElement = this.el.shadowRoot.querySelector(
      ".conversation__comment span"
    );
    if (commentContent) {
      commentContent.classList.remove("conversation__comment--translate");
    }
  }

  private getFinalMessage() {
    if (!this.customFields["final_voting_message"]) {
      return this.DEFAULT_GOODBY_MSG;
    }
    let messageElement: HTMLElement =
      this.el.shadowRoot.querySelector("#end-message");
    if (messageElement) {
      messageElement.innerHTML = this.customFields["final_voting_message"];
    }
  }

  private getComponent() {
    return (
      <div>
        <ej-card
          commentHeight={this.commentHeight}
          childIsVisible={this.childIsVisible}
        >
          <div id="conversation">
            <div class="conversation__statistics">
              <div>
                <i class="fa-solid fa-comments"></i>
                <span>{this.conversation.statistics.comments.approved}</span>
              </div>
              <div>
                <i class="fa-solid fa-check-to-slot"></i>
                <span>{this.conversation.statistics.votes.total}</span>
              </div>
            </div>
            <div class="conversation__title">
              <div class="conversation__title-label">
                <span>Assunto</span>
              </div>
              <div class="conversation__title-text">
                <span>{this.conversation.text}</span>
              </div>
            </div>
            <div class="conversation__info">
              <div class="conversation__progress">
                <div class="percentage">
                  <span>{this.participationPercentage}</span>
                </div>
                <div class="progress-bar">
                  <div class="progress-bar__background"></div>
                  <div class="progress-bar__progress"></div>
                </div>
                <div class="comments-count">
                  <div>
                    <span>{this.getParticipationComments()}</span>
                  </div>
                </div>
              </div>
              <div
                class="comments-new"
                onClick={() => this.renderCommentsComponent()}
              >
                {" "}
                <div>
                  <i class="fa-solid fa-circle-plus"></i>
                </div>
                <div>
                  <span>adicionar comentário</span>
                </div>
              </div>
            </div>
            <div class="conversation__comment-label">
              <span>Comentário para votação</span>
            </div>
            <div class="conversation__comment">
              {this.comment.content && <span>{this.comment.content}</span>}
              <span id="end-message">{this.getFinalMessage()}</span>
            </div>
            <div class="conversation__buttons flex flex-justify-content-between">
              <vote-button
                choice="agree"
                text="concordo"
                classes="fa-solid fa-check"
              ></vote-button>
              <vote-button
                choice="skip"
                text="pular"
                classes="fa-solid fa-arrow-right"
              ></vote-button>
              <vote-button
                choice="disagree"
                text="discordo"
                classes="fa-solid fa-xmark"
              ></vote-button>
            </div>
            <span class="disclaimer-text">
              Os comentários acima representam a visão dos participantes
            </span>
          </div>
        </ej-card>
      </div>
    );
  }

  render() {
    return (
      <div>
        {User.isAuthenticated() &&
          this.componentIsVisible &&
          this.getComponent()}
      </div>
    );
  }
}
