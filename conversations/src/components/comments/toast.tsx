import { Component, h, Prop, Element } from "@stencil/core";
import { Listen } from "@stencil/core";
import { HTMLStencilElement } from "@stencil/core/internal";
import { Event, EventEmitter } from "@stencil/core";

@Component({
  tag: "ej-comments-toast",
  styleUrls: ["./main.css"],
  shadow: true,
  assetsDirs: ["assets"],
})
export class CommentToast {
  @Prop({ mutable: true }) showComponent: boolean = false;
  @Element() el!: HTMLStencilElement;
  @Event({ bubbles: true, composed: true })
  resetToast: EventEmitter<any>;

  @Listen("renderToastComponent", { target: "window" })
  async openComponentHandler() {
    this.showComponent = true;
  }

  componentDidRender() {
    if (this.showComponent) {
      setTimeout(() => {
        this.el.shadowRoot.querySelector("div").classList.remove("toast--show");
        this.showComponent = false;
      }, 2500);
    }
  }

  render() {
    if (!this.showComponent) {
      return;
    }
    return (
      <div class={`toast ${this.showComponent ? "toast--show" : ""}`}>
        Comentário enviado para moderação!
      </div>
    );
  }
}
