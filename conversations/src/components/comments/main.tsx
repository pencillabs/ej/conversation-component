import { Component, h, Prop, Element } from "@stencil/core";
import { Listen } from "@stencil/core";
import { Conversation } from "../conversation/api/conversation";
import { User, UserCommentsStatistics } from "../conversation/api/user";
import { API } from "../conversation/api/api";
import { HTMLStencilElement } from "@stencil/core/internal";
import { Event, EventEmitter } from "@stencil/core";

@Component({
  tag: "ej-conversation-comments",
  styleUrls: ["./main.css"],
  shadow: true,
  assetsDirs: ["assets"],
})
export class EjConversationComments {
  @Prop({ mutable: true }) componentIsVisible: boolean = false;
  @Prop({ mutable: true }) conversation: Conversation = null;
  @Prop({ mutable: true }) user: User = null;
  @Prop({ mutable: true }) userCommentsStatistics: UserCommentsStatistics =
    null;
  @Prop({ mutable: true }) comment: string = "";
  @Prop({ mutable: true }) errorMsg: string = "";
  @Element() el!: HTMLStencilElement;
  @Prop() api: API;
  @Prop() childIsVisible: boolean = false;
  @Event({
    eventName: "renderVotesComponent",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  renderVotesComponent: EventEmitter;
  @Event({
    eventName: "renderToastComponent",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  renderToastComponent: EventEmitter;

  @Listen("renderCommentComponent", { target: "window" })
  async openComponentHandler(event: CustomEvent<any>) {
    this.conversation = event.detail["conversation"];
    this.user = event.detail["user"];
    this.userCommentsStatistics = await this.api.getUserCommentsStatistics();
    this.componentIsVisible = !this.componentIsVisible;
    this.childIsVisible = true;
  }

  cleanTextArea() {
    this.comment = "";
    this.el.shadowRoot.querySelector("textarea").value = "";
  }

  closeComponent() {
    this.componentIsVisible = false;
    this.cleanTextArea();
    this.childIsVisible = true;
    this.renderVotesComponent.emit();
  }

  async save() {
    this.validateComment();
    if (this.errorMsg) {
      return;
    }
    try {
      await this.api.createComment(this.comment);
      this.userCommentsStatistics = await this.api.getUserCommentsStatistics();
      this.renderToastComponent.emit();
      setTimeout(() => {
        this.cleanTextArea();
      }, 2000);
    } catch (e) {
      this.errorMsg =
        "Esse comentário já existe na conversa. Tente um texto diferente e envie novamente.";
    }
  }

  maxCommentPerUser(): boolean {
    if (this.userCommentsStatistics.createdComments == 2) {
      return true;
    }
    return false;
  }

  setComment(event: any) {
    this.comment = event.target.value;
  }

  private validateComment() {
    if (this.comment.length <= 2) {
      this.errorMsg = "Comentário precisa ter no mínimo 3 caracteres.";
      throw Error;
    }
    if (this.comment.length >= 252) {
      this.errorMsg = "Comentário pode ter no máximo 252 caracteres.";
      throw Error;
    }
    this.errorMsg = "";
  }

  private getComponent() {
    return (
      <div>
        <ej-card
          childIsVisible={this.childIsVisible}
          show={this.componentIsVisible}
          commentHeight={60}
        >
          {this.conversation && (
            <div id="comments">
              <div class="conversation__statistics">
                <div>
                  <i class="fa-solid fa-comments"></i>
                  <span>{this.conversation.statistics.comments.approved}</span>
                </div>
                <div>
                  <i class="fa-solid fa-check-to-slot"></i>
                  <span>{this.conversation.statistics.comments.total}</span>
                </div>
              </div>
              <div class="conversation__title">
                <h2>{this.conversation.text}</h2>
              </div>
              <div class="comment-moderation">
                <div class="comment-moderation__label">
                  <span>adicionar comentário</span>
                </div>
                <div class="comment-moderation__counter">
                  <span>{this.userCommentsStatistics.createdComments}</span>
                </div>
              </div>
              <div class="comment-input">
                <textarea
                  maxLength={252}
                  onInput={(event) => this.setComment(event)}
                  placeholder="Adicione aqui a sua opinião. Ela será apresentada aos participantes da coleta."
                ></textarea>
                <div id="validation">{this.errorMsg}</div>
              </div>
              <div class="comment-input__max-size">
                <span>(máx. 252 caracteres)</span>
              </div>
              <div class="buttons">
                <div
                  onClick={() => this.closeComponent()}
                  class="close-card form__button action-button"
                >
                  <button>voltar</button>
                </div>
                <div
                  onClick={() => this.save()}
                  class="comment-action-button form__button action-button"
                >
                  <button
                    type="button"
                    class={
                      this.maxCommentPerUser()
                        ? "button--disabled action-button--disabled"
                        : ""
                    }
                    disabled={this.maxCommentPerUser()}
                  >
                    enviar para moderação
                  </button>
                </div>
              </div>
            </div>
          )}
          <ej-comments-toast></ej-comments-toast>
        </ej-card>
      </div>
    );
  }

  render() {
    return <div>{this.componentIsVisible && this.getComponent()}</div>;
  }
}
