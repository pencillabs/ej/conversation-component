import {
  Component,
  Prop,
  h,
  State,
  Listen,
  Event,
  EventEmitter,
} from "@stencil/core";

@Component({
  tag: "vote-button",
})
export class VoteButton {
  @Prop() choice: string;
  @Prop() text: string;
  @Prop() classes: string;

  @State() loading: boolean = false;

  @Event({
    eventName: "voteButtonPressed",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  voteButtonPressed: EventEmitter;

  voteButtonPressedHandler(choice: string) {
    this.loading = true;
    this.voteButtonPressed.emit(choice);
  }

  @Listen("voted", { target: "window" })
  async votedHandler() {
    this.loading = false;
  }

  render() {
    return (
      <button
        id={this.choice}
        onClick={() => this.voteButtonPressedHandler(this.choice)}
      >
        {this.loading ? (
          <span>
            <i class="fas fa-circle-notch fa-spin"></i>
          </span>
        ) : (
          <span>
            <i class={this.classes}></i> <span>{this.text}</span>
          </span>
        )}
      </button>
    );
  }
}
