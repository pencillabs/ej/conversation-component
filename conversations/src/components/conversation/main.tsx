import { Component, Prop, h, Element } from "@stencil/core";
import { User } from "./api/user";
import { API } from "./api/api";
import { HTMLStencilElement } from "@stencil/core/internal";

export interface QueryParams {
  commentId: string;
  conversationId: string;
  votingChoice: string;
  email: string;
}

export interface CustomFields {
  background_image_url: string;
  logo_image_url: string;
  final_voting_message: string;
}

@Component({
  tag: "ej-conversation",
  styleUrls: ["main.css", "assets/css/all.css"],
  shadow: true,
  assetsDirs: ["assets"],
})
export class EjConversation {
  @Element() el!: HTMLStencilElement;
  @Prop() host: string = "";
  @Prop() cid: string = "";
  @Prop() authenticateWith: string = "register";
  @Prop() conversationAuthorToken: string = "";
  @Prop() user: User = new User();
  @Prop() api: API;
  @Prop() overlayMode: boolean = false;
  @Prop({ mutable: true }) customFields: CustomFields = {
    background_image_url: "",
    logo_image_url: "",
    final_voting_message: "",
  };
  @Prop() queryParams: QueryParams = {
    commentId: "",
    conversationId: "",
    votingChoice: "",
    email: "",
  };

  componentDidRender() {
    this.checkHowToRender();
  }

  async componentWillLoad() {
    this.setQueryParams(document.location.search);
    if (this.queryParams) {
      this.api = new API(
        this.host,
        this.cid,
        this.authenticateWith,
        this.queryParams.commentId
      );
    } else {
      this.api = new API(this.host, this.cid, this.authenticateWith);
    }
    this.customFields = await this.api.getCustomFields();
  }

  renderWithOverlay(backgroundElement: HTMLElement) {
    document.body.insertBefore(this.el, document.body.firstChild);
    const mainElement: HTMLElement = this.el.shadowRoot.querySelector("main");
    mainElement.style.position = "unset";
    const bodyElement = document.body;
    // this hack is necessary because bodyElement does not exists inside EjConversation component
    bodyElement.style.maxHeight = "100vh";
    bodyElement.style.overflow = "hidden";
    backgroundElement.classList.add("background--overlay");
  }

  async renderWithBackground(backgroundElement: HTMLElement) {
    backgroundElement.style.backgroundImage = `url(${this.customFields["background_image_url"]})`;
  }

  /**
   * checkHowToRender controls how ej-conversation will appear on page.
   * this.overlayMode will make the component to appear ahead of page content.
   */
  checkHowToRender() {
    const backgroundElement: HTMLElement =
      this.el.shadowRoot.querySelector(".background");
    if (this.overlayMode) {
      this.renderWithOverlay(backgroundElement);
    } else {
      this.renderWithBackground(backgroundElement);
    }
  }

  private setQueryParams(documentLocationSearch: string) {
    if (documentLocationSearch != "/" && documentLocationSearch != "") {
      let queryParams: string[] = documentLocationSearch
        .replace("?", "")
        .split("&");
      for (let param of queryParams) {
        if (param.match(/.*cid.*/)) {
          this.queryParams.conversationId = param.split("=")[1];
        }
        if (param.match(/^comment_id/)) {
          this.queryParams.commentId = param.split("=")[1];
        }
        if (param.match(/^choice/)) {
          this.queryParams.votingChoice = param.split("=")[1];
        }
        if (param.match(/.*email.*/)) {
          this.queryParams.email = param.split("=")[1];
        }
      }
    }
  }

  render() {
    return (
      <main>
        <div class="background"></div>
        <div id="components">
          <ej-conversation-register
            host={this.host}
            user={this.user}
            api={this.api}
            customFields={this.customFields}
          ></ej-conversation-register>
          <ej-conversation-votes
            host={this.host}
            user={this.user}
            api={this.api}
            queryParams={this.queryParams}
            customFields={this.customFields}
          ></ej-conversation-votes>
          <ej-conversation-comments api={this.api}></ej-conversation-comments>
          <ej-conversation-login
            host={this.host}
            user={this.user}
            api={this.api}
          ></ej-conversation-login>
        </div>
      </main>
    );
  }
}
