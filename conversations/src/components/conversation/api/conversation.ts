interface ConversationVotesStats {
  agree: number;
  disagree: number;
  skip: number;
  total: number;
}

interface ConversationCommentsStats {
  approved: number;
  pending: number;
  rejected: number;
  total: number;
}

interface ConversationStatistics {
  channel_participants: any;
  channel_votes: any;
  comments: ConversationCommentsStats;
  participants: any;
  votes: ConversationVotesStats;
}

export class Conversation {
  text: string;
  statistics: ConversationStatistics;

  constructor(text: string, statistics: ConversationStatistics) {
    this.text = text;
    this.statistics = statistics;
  }
}
