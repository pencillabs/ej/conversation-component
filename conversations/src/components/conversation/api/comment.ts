interface CommentLink {
  self: string;
}

export class Comment {
  content: string;
  created: string;
  links: CommentLink;
  rejection_reason: number;
  rejection_reason_text: string;
  status: string;
  id: string;

  constructor(
    content: string,
    links: any,
    created?: string,
    rejection_reason?: number,
    rejection_reason_text?: string,
    status?: string
  ) {
    this.content = content;
    this.links = links;
    this.created = created;
    this.rejection_reason = rejection_reason;
    this.rejection_reason_text = rejection_reason_text;
    this.status = status;
    this.getIdFromlinks();
  }

  getIdFromlinks() {
    if (this.links) {
      let tokens: string[] = this.links.self.split("comments");
      if (tokens) {
        let idToken: string = tokens[1];
        if (idToken) {
          this.id = idToken.replace(/\//g, "");
        }
      }
    }
  }

  toJSON() {
    return {
      content: this.content,
      links: this.links,
      created: this.created,
      rejection_reason: this.rejection_reason,
      rejection_reason_text: this.rejection_reason_text,
      status: this.status,
      id: this.id,
    };
  }
}
