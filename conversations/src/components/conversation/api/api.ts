import { APIConfig } from "./api_config";
import { User } from "./user";
import { v4 as uuidv4 } from "uuid";
import { Comment } from "./comment";

export class API {
  config: APIConfig;
  COMMENTS_PER_USER: number = 2;
  USER_FACTORY_OPTIONS: any = {
    mautic: this.buildUserUsingMauticID,
    analytics: this.buildUserUsingAnalyticsID,
    register: this.getUser,
    default: this.buildUserUsingDefaultID,
  };
  authMethod: string = "default";
  conversationId: string = "";
  constructor(
    host: string,
    conversationId: string,
    authMethod: string,
    commentID?: string
  ) {
    this.config = new APIConfig(host, conversationId, commentID);
    this.authMethod = authMethod;
    this.conversationId = conversationId;
  }

  async authenticate() {
    let buildUserMethod = this.USER_FACTORY_OPTIONS[this.authMethod];
    let user: User = buildUserMethod.bind(this)();
    if (user) {
      let response = await this.createUser(user);
      user.token = response.token;
      user.save();
      return user;
    } else {
      throw new Error("could not authenticate user");
    }
  }

  async getConversation() {
    let response = await this.httpRequest(this.config.CONVERSATIONS_ROUTE);
    if (response.status) {
      return { response: response, status: response.status };
    }
    return { response: response, status: 200 };
  }

  async getUserConversationStatistics() {
    return this.httpRequest(this.config.USER_STATISTICS_ROUTE);
  }

  async updateUserProfile(payload: any) {
    return this.httpRequest(this.config.PROFILE_ROUTE, JSON.stringify(payload));
  }

  async getConversationClusters(clusterizationLink: string) {
    return this.httpRequest(clusterizationLink + "clusters");
  }

  async getConversationNextComment(): Promise<any> {
    let commentUrl: string = "";
    if (this.config.COMMENT_ROUTE) {
      commentUrl = this.config.COMMENT_ROUTE;
    } else {
      commentUrl = this.getRandomCommentUrl();
    }
    let { content, links } = await this.httpRequest(commentUrl);
    let comment: Comment = new Comment(content, links);
    return comment.toJSON();
  }

  async userCanAddNewComment() {
    let stats = await this.getUserCommentsStatistics();
    if (stats.createdComments >= this.COMMENTS_PER_USER) {
      return false;
    }
    return true;
  }

  async getUserCommentsStatistics() {
    let created: number = await this.getUserCreatedCommentsCount();
    let pending: number = await this.getUserPendingCommentsCount();
    return { createdComments: created, pendingComments: pending };
  }

  async getUserCreatedCommentsCount() {
    const response = await this.httpRequest(this.config.USER_COMMENTS_ROUTE);
    return response.length;
  }

  async getUserPendingCommentsCount() {
    const response = await this.httpRequest(
      this.config.USER_PENDING_COMMENTS_ROUTE
    );
    return response.length;
  }

  async vote(comment: any, choice: string) {
    let body: string = JSON.stringify({
      comment: this.getCommentID(comment),
      choice: this.config.VOTE_CHOICES[choice],
      channel: "opinion_component",
    });
    await this.httpRequest(this.config.VOTES_ROUTE, body);
  }

  async createComment(content: any) {
    let data = {
      content: content,
      conversation: this.conversationId,
      status: "approved",
    };
    try {
      return await this.httpRequest(
        this.config.COMMENTS_ROUTE,
        JSON.stringify(data)
      );
    } catch (e) {
      throw Error;
    }
  }

  async getCustomFields() {
    try {
      return await this.httpRequest(
        this.config.OPINION_COMPONENT_CONFIGURATION_ROUTE
      );
    } catch (e) {
      throw Error;
    }
  }

  async createUser(user: User) {
    try {
      let response: any = await this.httpRequest(
        this.config.REGISTRATION_ROUTE,
        user.toJSON()
      );
      return response;
    } catch (error) {
      throw error;
    }
  }

  async authenticateUser(email, password) {
    const data = { email: email, password: password };
    try {
      let response: any = await this.httpRequest(
        this.config.AUTHENTICATE_ROUTE,
        JSON.stringify(data)
      );
      return response;
    } catch (error) {
      throw error;
    }
  }

  async httpRequest(route: string, payload?: string) {
    let requestOpts = this.getHttpRequestOpts(payload);
    let response: any = {};

    try {
      response = await fetch(route, requestOpts);
      let responseData = await response.json();

      if (response.ok) {
        return responseData;
      }

      if (response.status == 400 && "email" in responseData) {
        throw {
          apiError:
            "Seu email já tem cadastro, clique na opção 'Já sou usuário'.",
          status: 400,
        };
      }

      if (
        [400, 404, 500].includes(response.status) &&
        "error" in responseData
      ) {
        throw {
          apiError: responseData["error"],
          status: response.status,
        };
      }

      throw Error;
    } catch (error) {
      throw error;
    }
  }

  getUser() {
    let user = User.get();
    user.token = "";
    return user;
  }

  buildUserUsingMauticID() {
    let cookieName: string = this.config.COOKIES_MAP["mautic"];
    let cookie = this.getCookie(cookieName);
    if (cookie) {
      return new User(
        `Participante anônimo`,
        `${cookie}-mautic@mail.com`,
        `${cookie}-mautic`
      );
    }
  }

  buildUserUsingAnalyticsID() {
    let cookieName: string = this.config.COOKIES_MAP["analytics"];
    let cookie = this.getCookie(cookieName);
    if (cookie) {
      return new User(
        `Participante anônimo`,
        `${cookie}-analytics@mail.com`,
        `${cookie}-analytics`
      );
    }
  }

  buildUserUsingDefaultID() {
    let sessionName: string = "uuidv4";
    let sessionID = this.getSessionOrCreate(sessionName);
    if (sessionID) {
      return new User(
        `Participante anônimo`,
        `${sessionID}-${sessionName}@mail.com`,
        `${sessionID}-${sessionName}`
      );
    }
  }

  /*
   * This method will retrieve a cookie and use it as
   * username on EJ
   */
  getCookie(cookieName: string): string {
    let cookies = document.cookie;
    let cookieIndex = cookies.indexOf(cookieName);
    let cookieValue = "";
    if (cookieIndex != -1) {
      let cookieKeyAndValue = cookies.substring(cookieIndex, cookies.length);
      cookieValue = cookieKeyAndValue.split("=")[1];
      if (cookieValue.indexOf(";") != -1) {
        cookieValue = cookieValue.split(";")[0];
      }
    }
    return cookieValue;
  }

  getSessionOrCreate(sessionName: string): string {
    let user = sessionStorage.getItem(sessionName);
    if (!user) {
      try {
        const uid = uuidv4();
        sessionStorage.setItem(sessionName, uid);
      } catch (e) {
        console.log(e);
      }
    }
    return sessionStorage.getItem(sessionName);
  }

  getCommentID(comment: any): number {
    let selfLink = comment.links["self"];
    let linkAsArray = selfLink.split("/");
    return Number(linkAsArray[linkAsArray.length - 2]);
  }

  getRandomCommentUrl() {
    return this.config.CONVERSATION_RANDOM_COMMENT_ROUTE;
  }

  getApprovedCommentsUrl(conversation: any) {
    let comment: any = conversation.links["approved-comments"];
    try {
      if (this.config.HOST.split(":")[0] == "https") {
        return comment.replace("http", "https");
      }
      return comment;
    } catch (error) {
      return comment;
    }
  }

  private getHttpRequestOpts(payload: string) {
    let requestOpts: any = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    if (payload) {
      requestOpts["body"] = payload;
      requestOpts["method"] = "POST";
    }
    let user = User.get();
    if (user.token) {
      requestOpts.headers["Authorization"] = `Token ${user.token}`;
    }
    return requestOpts;
  }
}
