export interface UserCommentsStatistics {
  createdComments: number;
  pendingComments: number;
}

interface UserParticipation {
  votes: number;
  missing_votes: number;
  participation_ratio: number;
  total_comments: number;
  comments: number;
}

export class User {
  name: string;
  email: string;
  password: string;
  passwordConfirm: string;
  displayName: string;
  stats: any;
  token?: string;
  participation: UserParticipation;

  static invalidUserNameMessage: string =
    "Nome precisa ter cinco ou mais caracteres";

  constructor(email?: string, password?: string, token: string = "") {
    this.name = email;
    this.email = email;
    this.password = password;
    this.passwordConfirm = password;
    this.displayName = "";
    this.token = token;
    this.stats = {};
  }

  setPassword() {
    this.password = this.email;
    this.passwordConfirm = this.email;
  }

  save() {
    localStorage.setItem(
      "user",
      JSON.stringify({ name: this.name, email: this.email, token: this.token })
    );
  }

  nameIsVald() {
    return this.name.trim().length >= 5;
  }

  tokenIsNotValid() {
    return this.token ? false : true;
  }

  static get(token: string = "") {
    let user: User = new User();
    if (token) {
      user.token = token;
      user.name = token;
      user.email = `${token}@mail.com`;
      user.save();
      return user;
    }
    try {
      let userData: any = { ...JSON.parse(localStorage.getItem("user")) };
      user.name = userData.name;
      user.email = userData.email;
      user.token = userData.token;
    } catch (error) {
      console.log(error.message);
      console.log("user object not exists");
    }
    return user;
  }

  static isAuthenticated() {
    return localStorage.getItem("user") != null;
  }

  toJSON() {
    return JSON.stringify({
      name: this.name,
      email: this.email,
      password: this.password,
      password_confirm: this.passwordConfirm,
    });
  }
}
