import { newSpecPage } from "@stencil/core/testing";
import { EjConversation } from "../main";

describe.skip("test main components", () => {
  it("should render register component", async () => {
    const page = await newSpecPage({
      components: [EjConversation],
      html: `<ej-conversation host="" cid="" authenticate-with="register"></ej-conversation>`,
    });
    expect(page.root).toEqualHtml(`
     <ej-conversation authenticate-with="register" cid="" host="">
       <mock:shadow-root>
       <div>
        <ej-conversation-board theme="icd"></ej-conversation-board>
        <ej-conversation-register
          host=""
          theme="icd"
        ></ej-conversation-register>
        </div>
       </mock:shadow-root>
     </ej-conversation>
  `);
  });

  it("should render comments component", async () => {
    jest.mock("../api/api");
    const page = await newSpecPage({
      components: [EjConversation],
      html: `<ej-conversation host="" cid="" authenticate-with="register"></ej-conversation>`,
    });
    expect(page.root).toEqualHtml(`
    <ej-conversation authenticate-with="register" cid="" host="">
      <mock:shadow-root>
        <div>
          <ej-conversation-board theme="icd"></ej-conversation-board>
          <ej-conversation-register host="" theme="icd"></ej-conversation-register>
        </div>
      </mock:shadow-root>
    </ej-conversation>
  `);
  });
});
