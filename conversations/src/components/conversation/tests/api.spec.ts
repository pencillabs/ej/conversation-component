import { API } from "../api/api";
import { User } from "../api/user";

const userData: any = {
  token: "sometoken2",
  name: "teste",
  email: "teste@mail.com",
  password1: "teste1234",
  password2: "teste1234",
};

describe.skip("test api", () => {
  it("save user", () => {
    let fakeUser = new User(userData.name, userData.email, userData.password1);
    fakeUser.save();
    let savedUser = User.get();
    expect(savedUser.tokenIsNotValid()).toBe(false);
    expect(savedUser.token).toBe("sometoken2");
  });

  it("should check forced user token", () => {
    let fakeUser = User.get("123456");
    expect(fakeUser.tokenIsNotValid()).toBe(false);
    expect(fakeUser.token).toBe("123456");
  });

  it("should check user token", () => {
    localStorage.setItem("user", JSON.stringify(userData));
    let fakeUser = User.get();
    expect(fakeUser.tokenIsNotValid()).toBe(false);
  });

  it("should get user token", () => {
    localStorage.setItem("user", JSON.stringify(userData));
    let fakeUser = User.get();
    expect(fakeUser.token).toBe("sometoken2");
  });

  it("should return user when cookie exists", () => {
    const api = new API("http://localhost", "1", "register");
    api.getCookie = jest.fn().mockReturnValue("12345678");
    expect(api.buildUserUsingAnalyticsID().name).toBe("Participante anônimo");
  });

  it("should return user", () => {
    const api = new API("http://localhost", "1", "default");
    const key = "uuidv4";
    const user = api.buildUserUsingDefaultID();
    expect(user.email.includes(key)).toBeTruthy();
  });

  it("should return mautic cookie from a list of cookies", () => {
    const api = new API("http://localhost", "1", "register");
    document.cookie =
      "wp-settings-time-1=1582866339; mtc_id=9015; mtc_sid=t55crwrfus0r5eblxotzvzg; mautic_device_id=t55crwrfus0r5eblxotzvzg";
    let cookie: string = api.getCookie("mtc_id");
    expect(cookie).toBe("9015");
  });

  it("should return analytics cookie from a list of cookies", () => {
    const api = new API("http://localhost", "1", "register");
    document.cookie =
      "wp-settings-time-1=1582866339; mtc_id=9015; mtc_sid=t55crwrfus0r5eblxotzvzg; mautic_device_id=t55crwrfus0r5eblxotzvzg; _ga=GA.1.12345";
    let cookie: string = api.getCookie("_ga");
    expect(cookie).toBe("GA.1.12345");
  });

  it("should return empty string from a list of cookies", () => {
    const api = new API("http://localhost", "1", "register");
    document.cookie =
      "wp-settings-time-1=1582866339; mtc_sid=t55crwrfus0r5eblxotzvzg; mautic_device_id=t55crwrfus0r5eblxotzvzg";
    let cookie: string = api.getCookie("_ga");
    expect(cookie).toBe("");
  });

  it("should create a new uuidv4 session value", () => {
    const api = new API("http://localhost", "1", "default");
    const key = "uuidv4";
    expect(api.getSessionOrCreate(key)).toBeTruthy();
  });

  it("should create a new uuidv4 session value and dont recreat on new req", () => {
    const key = "uuidv4";

    const api1 = new API("http://localhost", "1", "default");
    api1.getSessionOrCreate(key);
    const s1 = sessionStorage.getItem(key);

    const api2 = new API("http://localhost", "1", "default");
    api2.getSessionOrCreate(key);
    const s2 = sessionStorage.getItem(key);

    expect(s1).toEqual(s2);
  });

  it("should return commentID from EJ data", () => {
    const api = new API("http://localhost", "1", "register");
    let comment: any = {
      links: { self: "http://localhost:8000/api/v1/comments/29/" },
      content: "Um novo comentário interessante",
      status: "approved",
      created: "2020-02-05T20:08:49.749127Z",
      rejection_reason: 0,
      rejection_reason_text: "",
    };
    let commentID: number = api.getCommentID(comment);
    expect(commentID).toBe(29);
  });

  it("should set data on API", () => {
    const api = new API("http://localhost", "1", "register");
    expect(api.conversationId).toBe("1");
    expect(api.config.HOST).toBe("http://localhost");
  });

  it("should return comment url from EJ data", () => {
    const api = new API("http://localhost", "1", "register");
    let commentURL: string = api.getRandomCommentUrl();
    expect(commentURL).toBe(
      "http://localhost/api/v1/conversations/1/random-comment/"
    );
  });

  it("should return comment url with https from EJ data", () => {
    const api = new API("https://localhost", "1", "register");
    let commentURL: string = api.getRandomCommentUrl();
    expect(commentURL).toBe(
      "https://localhost/api/v1/conversations/1/random-comment/"
    );
  });
});
