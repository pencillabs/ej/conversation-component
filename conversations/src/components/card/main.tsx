import { Component, Prop, h, Element } from "@stencil/core";
import { HTMLStencilElement } from "@stencil/core/internal";

@Component({
  tag: "ej-card",
  styleUrls: ["main.css"],
  shadow: true,
})
export class EjCard {
  @Prop() size: string = "";
  @Prop() show: boolean = true;
  @Prop() childIsVisible: boolean = false;
  @Prop({ mutable: true }) cardHeight: number;
  @Prop({ mutable: true }) commentHeight: number = 0;
  @Element() el!: HTMLStencilElement;

  /**
   * translateCard fix card position based on EJ comment Height.
   * This is necessary because card vertical align varies based comment Height.
   */
  private translateCard() {
    let card: HTMLElement = this.el.shadowRoot.querySelector(".card");
    let cardHeight: number = card.clientHeight;
    if (!this.commentHeight) {
      card.style.transform = `translate(-50%, calc(-50vh + ${
        cardHeight / 2
      }px))`;
    } else {
      card.style.transform = `translate(-50%, calc(-50vh + ${
        cardHeight / 2 + this.commentHeight / 10
      }px))`;
    }
  }

  /**
   * fixChildOpacity hides the translate animation.
   * When translateCard executes, the user can see the card on position 0 going to position 1.
   * To hide this positioning issue, fixChildOpacity hides the card while the translate executes.
   */
  private fixChildOpacity() {
    let card: HTMLElement = this.el.shadowRoot.querySelector(".card");
    card.style.opacity = "0";
    setTimeout(() => {
      card.style.opacity = "1";
    }, 500);
  }

  private checkBodyOverlay() {
    if (document.body.style.overflow == "hidden") {
      const cardElement = this.el.shadowRoot.querySelector(".card");
      cardElement.classList.add("card--overlay");
    }
  }
  componentDidRender() {
    this.checkBodyOverlay();
    if (this.childIsVisible) {
      this.fixChildOpacity();
    }
    this.translateCard();
  }

  private getComponent() {
    if (!this.show) {
      return;
    }
    return (
      <div class={`card ${this.size == "small" ? "card--small" : ""}`}>
        <slot />
      </div>
    );
  }

  render() {
    return <div>{this.getComponent()}</div>;
  }
}
