# Autenticação na API da EJ.

Atualmente existem três formas de autenticação na api da ej.

- Analytics
- Mautic
- Register

Quem promove uma nova coleta escolhe um das formas de autenticação passando uma string com o nome do método desejado, esse método é definido através da tag authenticate-with. Ex: (authenticate-with="analytics"). Além disso, nós tambem podemos alterar o tema do componente por meio da tag theme. Ex: (theme="icd"). Todas essas tags são informadas quando o componente é incluído na pagina html (no caso do ambiente local, no index.html).

A autenticação via mautic ou analytics se dá por meio de cookies, ou seja, caso o usuário deseje continuar participando da conversa de onde ele parou ele tem duas opções:
- Continuar a participação pelo mesmo computador onde a ação foi iniciada. (Caso o usuário use uma aba anônima ele será considerado como um novo usário e irá precisar se resgitrar novamente.)
- Registrar-se na EJ, por meio da conta que já foi criada (mudar os dados que foram gerados automáticos para os seus próprios dados.)

Já a autenticação via register o usuário vai se registrar ne EJ, e logo em seguida ele fará uma identificação via email e selecionar uma senha para a sua conta, cada usuário tem um token de identificação. 

Após a criação de um novo usuário, o componente ej-conversation é exibido, com base nos parâmetros setados no ej-conversation do arquivo index.html.

Os comentários que são exibidos estão relacionados com ID da conversa. 

O usuário pode gerar novos comentários e votar em comentários de outros participantes.

Quando um usuário gera novos comentários o número de comentários criados e pendentes deste usuário é atualizado, pois cada participante pode comentar duas vezes, todas essas ações são realizadas pelo componente comentários.

# ej-conversation-board

O primeiro componente que visualizamos ao abrir a página do site instituto cidade democrática é um ej-board, nele estão descritos os termos de uso e a política de privacidade para o uso do site, para que você consiga utilizar o site e participar das conversas é necessário executar a ação de concordar com a política de privacidade (lgpd).

A responsabilidade desse componente é exibir novos containers conforme ações são executadas.

# ej-conversation

É o componente responsável por exibir todas as informações relacionadas a uma conversa, seu tema, comentários, votos, grupos, board, footer é o header. Basicamente é o componente que agrupa todos os outros componentes citados e os exibe. Sua implementação se encontra no arquivo main.tsx.

# ej-conversation-votes

O componente de votos é responsável por controlar o processo votação. É ele quem apresenta os botões de concordar, discordar e pular, além de apresentar o conteúdo dos comentários enviadas para a EJ.

# ej-conversation-comments

O componente de comentários é responsável por atualizar o estado dos comentários de um usuário na API e por adicionar novos comentários.

Ele está diretamente relacionado com o componente ej-conversation, já que para participar de uma conversa queremos participar de uma conversa comentando e votando nos comentários de outros usuários.


# ej-conversation-register

O componente de registro é o componente responsável por registrar um novo usuário com nome e email. Quando esse usuário é criado ele pode gerar novos comentários e participar de outras conversas. É o componente se é exibido após aceitação dos termos de uso do site.

# ej-conversation-groups

O componente de grupo só pode existir se uma determinada tem no mínimo 500 votos, ele irá mostrar os comentários negativos e positivos daquele grupo. Caso a conversa não tenha votos o suficiente ele mostra um aviso para o usuário.

# ej-conversation-header

Mostra o texto de uma conversa e as estátisticas (comentários e números de votos), da conversa em questão. 

# ej-comments-footer

Mostra quantos comentários o usuário ainda pode fazer e o número de comentários que faltam ser aprovados pela moderação.

# connection-error

Componente que dispara um erro sempre que ocorrem erros de conexão com a API da ej. Atualmente esse componente é chamado nos componentes ej-conversation-comments e no ej-conversation-register, em cada componente onde o connection-error é chamado é possível personalizar a mensagem de erro que irá aparecer para o usuário.
