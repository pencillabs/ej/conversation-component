import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'conversations',
  globalStyle: 'src/components/global/global.css',
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader'
    },
    {
      type: 'www',
      serviceWorker: null // disable service workers
    }
  ]
};
