O Componente de opinião é um *Web component* desenvolvido em [Stenciljs](https://stenciljs.com/). Seu objetivo é permitir que usuários votem nas conversas da EJ sem precisar acessar diretamente as instâncias de produção (https://ejplataform.org e https://ejparticipe.org). Isso é feito integrando o componente de opinião em páginas HTML como plataformas web, sites e blogs.

Para integrar o componente em uma página HTML, é preciso adicionar o pacote NPM via tag `script` e adicionar a `tag` que carrega o *web component*. Por exemplo:

```html
<!DOCTYPE html>
<html>
  <head>
    <script
      src="https://unpkg.com/ej-conversations/dist/conversations/conversations.esm.js"
      type="module"
    ></script>
    <link
      rel="stylesheet"
      href="https://unpkg.com/ej-conversations/dist/conversations/conversations.css"
    />
    <title>Pesquisa de opinião do Comitê Gestor da Internet (CGI)</title>
  </head>
  <body style="margin: unset !important">
    <ej-conversation
      conversation-author-token=""
      host="https://www.ejplatform.org"
	  overlay-mode="false"
      cid="126"
      background-image="https://gitlab.com/pencillabs/ej/ej-application/uploads/7b6e5e342b8d0606c2dbc4041e4eda79/tela-enquete2.png"
    ></ej-conversation>
  </body>
</html>
```  


# Desenvolvimento

Antes de executar o componente, é preciso ter uma [instância da EJ rodando](https://gitlab.com/pencillabs/ej/ej-application/-/blob/develop/README.rst). O componente possui algumas variáveis que precisam ser definidas para o seu correto funcionamento.

- **host**: O endereço da instância da EJ que será utilizada. Por exemplo: `http://localhost:8000` ou `https://ejplatform.org`.
- **cid**: O ID da conversa da EJ que iremos disponibilizar para votação.
- **background-image**: URL para uma imagem que será utilizada como imagem de fundo pelo componente.
- **overlay-mode**: Uma variável booleana, que define se o componente deve ser posicionado sobre o conteúdo da página HTML ou não.

Essas configurações podem ser feitas no arquivo `src/index.html`. Para subir o servidor de desenvolvimento do StencilJS, execute:

	sudo npm install @stencil/core@latest --save-exact -g
	npm install
	npm start

Para executar os testes:

	make test


Para publicar uma nova versão do componente, faça as alterações necessárias,
incremente a versão atual no arquivo `package.json` e utilize a task `make publish` para enviar a nova versão para o registry do NPM.

## Distribuição

Para o usuário final, o componente é distribuido via pacote npm, e pode ser
encontrado no link [https://www.npmjs.com/package/ej-conversations](https://www.npmjs.com/package/ej-conversations).

Para mais informações, acesse a [documentação da EJ para a ferramenta componente de opinião](https://www.ejplatform.org/docs/user-guides/pt-br/tools-opinion-component.html).